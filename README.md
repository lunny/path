# Golang package to operate path

[![Build Status](https://drone.gitea.com/api/badges/lunny/path/status.svg)](https://drone.gitea.com/lunny/path) [![](http://gocover.io/_badge/gitea.com/lunny/path)](http://gocover.io/gitea.com/lunny/path)
[![](https://goreportcard.com/badge/github.com/lunny/path)](https://goreportcard.com/report/gitea.com/lunny/path)

## Installation

```
go get gitea.com/lunny/path
```

## Usage

```Go
path.IsDir("./")
```