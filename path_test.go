package path

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMove(t *testing.T) {
	err := Move("./path_test.go", "./path_test.go.bak")
	assert.NoError(t, err)

	err = Move("./path_test.go.bak", "./path_test.go")
	assert.NoError(t, err)
}